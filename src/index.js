import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavBar from './components/AppNavBar';
// Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'


/*createRoot - assigns to the elements to be managed by React with its virtual DOM

render() - display the react elements/components
App component is our mother component, this is the component we use as an entry point and where we can render all other components or pages
*/
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);