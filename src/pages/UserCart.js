import { useState } from 'react'

import { Table } from 'react-bootstrap'


export default function UserOrders () {

    const [getOrders, setGetOrders] = useState();

    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
        headers : {
            'Content-Type' : 'application/json',
            Authorization : `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(response => response.json())
    .then(data => {
        // console.log(data)
        setGetOrders(data)
    })

    return (


        <>
            <h1>Users Orders</h1>
            <Table bordered>
                <thead>
                    <tr>
                        <th>Order</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>sdf</td>
                    </tr>
                </tbody>
            </Table>
        </>

    )
}