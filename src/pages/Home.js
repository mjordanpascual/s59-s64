import { useState, useEffect, useContext } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import UserContext from '../UserContext';
// import { UserProvider } from './UserContext';

export default function Home() {

    const { user }  = useContext(UserContext);
    const [users, setUsers] = useState('');

    useEffect( () => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers : {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(response => response.json())
        .then(data => {
          setUsers(data)
        })

    }, [])

	return (

		<>

			{
			(user.id !== null && user.id != undefined)
			?
				<p className="text-muted text-center m-0 p-1 fw-bold">Welcome {users.isAdmin ? 'admin' : 'user'}, <strong style={{letterSpacing: '1.1px'}}>{users.firstName + ' ' + users.lastName}</strong></p>
			:
				<></>
			}

			<Banner />
			{/* <Highlights /> */}
		</>

	)
}