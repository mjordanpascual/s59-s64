import { useEffect } from 'react';
import { Button, Container, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import ProductList from '../components/ProductList'

export default function Dashboard() {

	useEffect(() => {
		document.title = "Dashboard"
	}, [])



	return (

		<>
		<div className='d-flex flex-column-reverse mt-3'>
			<Button as={Link} to="/orderhistory" className='btn-sm' style={{width: '8rem'}}>Order History</Button>
			<Button as={Link} to="/userprofile" className='btn-sm mb-1' style={{width: '8rem'}}>User Profile</Button>
		</div>
		<Container class="col-sm-12 col-md-12"style={{textAlign: "center"}}>
			<Row className="mx-auto py-3">
				<Col>
						<h1>Admin Dashboard</h1>
					<div className="text-center">
						{/*<Button className="btn-sm" variant="success" onClick={handleAddProduct}>Add Product</Button>*/}
						<Button className="btn-sm" style={{ margin: '0 10px' }} variant="success" as={Link} to={`/addproduct`}>Add New Product</Button>
						<Button className="btn-sm" variant="primary" as={Link} to={`/products`}>See all products</Button>	
					</div>
				</Col>
			</Row>
		</Container>
			<ProductList />
			{/*<Products />*/}
		</>

	)

};
