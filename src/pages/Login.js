import { useState, useEffect, useContext } from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom'

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS 
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext';

export default function Login() {

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to determine whether submit buttin is enabled or not

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect( () => {

		if(email !== '' && password !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password])


	// Allows us to consume the UserContext object and it's properties to use for user validation

	const { user, setUser }  = useContext(UserContext);


	// const [user, setUser] = useState(localStorage.getItem('email'));


	function authenticate(e){
		e.preventDefault();

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('property', value);

		// The localStorage.setItem() allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering.

		// Because REACT JS is a single page application, using the localStorage will not trigger rerendering of component.

		// localStorage.setItem('email', email);
		// localStorage.setItem('password', password);


		// setUser(localStorage.getItem('email'));

		// alert('You are Logged In!');

		// navigate('/courses');
		
		// setEmail('');
		// setPassword('');

		// Process fetch request to the corresponding backend API
		// Syntax:
			 /*fetch('url', {options}).then(response => response.json()).then(
				data => {})
			*/

			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email : email,
					password : password
				})
			})
			.then(response => response.json())
			.then(data => {

				if(data === false){
					let timerInterval
					Swal2.fire({
						title: 'Login unsuccessful!',
						icon: 'error',
						  html: 'Please try again in <b></b> milliseconds.',
						  timer: 2500,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})


				} else if (data.isAdmin == true){
					// localStorage.setItem('token', data.access)
					
					// retrieveUserDetails(data.access)

					let timerInterval
					Swal2.fire({
						title: `Welcome, ${email}`,
						icon: 'success',
						  html: 'You will redirecting to Dashboard page in <b></b>.',
						  timer: 2500,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})
					
				navigate('/dashboard')

				} else {
					localStorage.setItem('token', data.access)
					
					retrieveUserDetails(data.access)

					let timerInterval
					Swal2.fire({
						title: `Welcome, ${email}`,
						icon: 'success',
						  html: 'You will redirecting to home page in <b></b>.',
						  timer: 2500,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})
					
					navigate('/')
				}


			})

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers : {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	return ( 
		  	!user.id
		  	?
		  		  	<Row xl={3} lg={2} md={2} sm={12} xs={12} className="pt-3 text-dark mx-auto">
		  		  		<Col className="bg-light px-5 mx-auto rounded shadow" >
		  		  			<h3 className="text-start mt-3 mb-4"><h3>Welcome to My Boutique!</h3><h4>Please Sign in.</h4></h3>
		  		  			<Form onSubmit = {event => authenticate(event)}>
		  					      <Form.Group className="mb-3" controlId="formBasicEmail">
		  					        <Form.Label>Email</Form.Label>
		  					        <Form.Control
		  					        type="email" 
		  					        placeholder="Enter email"
		  					        value={email}
		  					        onChange={e => setEmail(e.target.value)}
		  					         />
		  					      </Form.Group>

		  					      <Form.Group className="mb-3" controlId="formBasicPassword1">
		  					        <Form.Label>Password</Form.Label>
		  					        <Form.Control
		  					        type="password"
		  					        placeholder="Password"
		  					        value={password}
		  					        onChange={e => setPassword(e.target.value)}
		  					        />
		  					      </Form.Group>

		  					      <Button 
		  					      className="w-100" 
		  					      variant="primary" 
		  					      type="submit"
		  					      disabled = {isDisabled}
		  					      >
		  					       Login
		  					      </Button>
		  					      <Form.Group className="text-center text-dark">
		  					      	<p>Forgot password?</p>
		  					      </Form.Group>
		  					      <Form.Group className="mt-3 text-center" controlId="formBasicPassword1">

		  					        <label>New customer? <Link className='text-warning' as={Link} to={'/register'}>Create your account</Link></label>
		  					        <br/>
		  					        <br/>
		  					        <label className="mb-4">All Rights Reserved.</label>
		  					      </Form.Group>
		  		  			</Form>
		  		  		</Col>
		  		  	</Row>
		  	:
		  	<Navigate to = '/*' />
		)
}
