import { useEffect } from 'react';
import { Table, Button, Container, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import Products from '../pages/Products';
import ProductList from '../components/ProductList'

import AddProduct from './AddProduct';

export default function Dashboard() {

	useEffect(() => {
		document.title = "Cart Page"
	}, [])


	fetch(`${process.env.REACT_APP_API_URL}/users/`)


	return (

		<Row>
			<Col>
				<h1 className="text-center">Your Orders</h1>
				<Table striped bordered>
					<thead className="text-center">
						<tr>
							<th>Products</th>
							<th>Order ID</th>
							<th colSpan={2}>Action/Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Test Product Data</td>
							<td>54135437sdf54235fdsdf35sd4f</td>
								
								<td>Waiting for payment...</td>
								<td>Pending</td>
						</tr>
					</tbody>
				</Table>
			</Col>
		</Row>
			
	)
};

