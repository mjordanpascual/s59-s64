import { Card, Button, Row, Col, Form } from 'react-bootstrap'
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { useState, useEffect } from 'react'

import Swal2 from 'sweetalert2'


export default function AddProduct () {

	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	const [pName, setPName] = useState('');
	const [pDescription, setPDescription] = useState('');
	const [pPrice, setPPrice] = useState('');
	const [pImageUrl, setPImageUrl] = useState('');


	useEffect(() => {
		document.title = "Add Product"
	}, [])


	useEffect( () => {
		if( 
			pName !== '' && 
			pDescription !== '' && 
			pPrice !== '')
		{
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [ pName, pDescription, pPrice ])


		function handleProductSubmit (e) {
			e.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: pName,
					description: pDescription,
					price: pPrice,					
					imageUrl: pImageUrl
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data === false) {
					let timerInterval
					Swal2.fire({
						title: 'Product Something went wrong!',
						icon: 'error',
						  html: 'I will close in <b></b> milliseconds.',
						  timer: 3000,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})	
					} else {
						let timerInterval
							Swal2.fire({
								title: 'New product succesfully added',
								icon: 'success',
								  html: 'I will close in <b></b> milliseconds.',
								  timer: 3000,
								  timerProgressBar: true,
								  didOpen: () => {
								    Swal2.showLoading()
								    const b = Swal2.getHtmlContainer().querySelector('b')
								    timerInterval = setInterval(() => {
								      b.textContent = Swal2.getTimerLeft()
								    }, 100)
								  },
								  willClose: () => {
								    clearInterval(timerInterval)
								  }
								})

						navigate('/dashboard');	
					}
				})
			setPName('');
			setPDescription('');
			setPPrice('');
}


	return (

		  	  	<Row className='mt-5'>
		  	  		<Col className="col-6 mx-auto">
						<div className='d-flex'>
							<Button className='btn-sm align-items-center btn-outline-info h-25' as = {Link} to={'/dashboard'} style={{marginRight: '6.5rem'}}>{`<`}</Button>
		  	  				<h1>Add New Product</h1>	
						</div>
		  	  			<Form onSubmit={e => handleProductSubmit(e)}>
		  				     
			  	  				<Form.Group className="mb-3" controlId="formBasicPassword1">
			  	  				  <Form.Label>Product Name</Form.Label>
			  	  				  <Form.Control
			  	  				  type="text" 
			  	  				  placeholder="Enter Product Name"
			  	  				  value={pName}
			  	  				  onChange={e => setPName(e.target.value)}
			  	  				  />
			  	  				</Form.Group>

			  	  				<Form.Group className="mb-3" controlId="formBasicPassword2">
			  	  				  <Form.Label>Description</Form.Label>
			  	  				  <Form.Control
			  	  				  type="text" 
			  	  				  placeholder="Enter description"
			  	  				  value={pDescription}
			  	  				  onChange={e => setPDescription(e.target.value)}
			  	  				  />
			  	  				</Form.Group>

			  	  				<Form.Group className="mb-3" controlId="formBasicPassword3">
			  	  				  <Form.Label>Unit Price</Form.Label>
			  	  				  <Form.Control
			  	  				  type="number" 
			  	  				  placeholder="Enter Unit Price"
			  	  				  value={pPrice}
			  	  				  onChange={e => setPPrice(e.target.value)}
			  	  				  />
			  	  				</Form.Group>
			  	  				<Form.Group className="mb-3" controlId="formBasicPassword3">
			  	  				  <Form.Label>Image URL</Form.Label>
			  	  				  <Form.Control
			  	  				  type="string" 
			  	  				  placeholder="Enter URL Image"
			  	  				  value={pImageUrl}
			  	  				  onChange={e => setPImageUrl(e.target.value)}
			  	  				  />
			  	  				</Form.Group>
		  				      <Button 
		  				      className="w-100"
		  				      variant="primary" 
		  				      type="submit"
		  				      disabled = {isDisabled}
		  				      >Add Product</Button>	
		  				      
		  	  			</Form>
		  	  		</Col>
		  	  	</Row>

	)
}