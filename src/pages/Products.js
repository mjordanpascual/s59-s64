import { useState, useEffect, useContext } from 'react'
import ProductCard from '../components/ProductCard';
import { Container, Row, Col } from 'react-bootstrap'

import UserContext from '../UserContext';

export default function Products() {
	const { user }  = useContext(UserContext);
	const [users, setUsers] = useState('');

	const [products, setProducts] = useState([]);

	useEffect( () =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(data => {
					setProducts(data.map(product => {
						return (
							<ProductCard key={product._id} productProp={product} />
						)
					}))
				})
			}, [])

	useEffect(() => {
		document.title = "All Products";
	}, [])

	return (

		// <>
		// <Container className="text-center">
		// 	<Row>
		// 		<Col>
		// 			<h1>Products</h1>
		// 			<div className="grid">
		// 				{ products }	
		// 			</div>
		// 		</Col>
		// 	</Row>
		// </Container>
		
		// </>

		// <div className='text-center'>
		// 	<h1>Products</h1>
		// 	<d className='grid'>
		// 		{ products }	
		// 	</d	iv>
		// </div>
		
		<div>
			<h1>Products</h1>
			{ products }	
		</div>


	)
}