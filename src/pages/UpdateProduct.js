import { Button, Row, Col, Form } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'

import { useParams, useNavigate, Link } from 'react-router-dom';

// import UserContext from '../UserContext';
import Swal2 from 'sweetalert2';

// import ProductCard from "../components/ProductCard";

export default function UpdateProduct () {

		useEffect(() => {
			document.title = "Product Update"
		}, [])

		const navigate = useNavigate()
		const { productId } = useParams();

		const [productName, setProductName] = useState('');
		const [productDesc, setProductDesc] = useState('');
		const [productPrice, setProductPrice] = useState('');
		const [productImageUrl, setProductImageUrl] = useState('');

		const [isDisabled, setIsDisabled] = useState(true);

		useEffect( () => {
			if(	productName !== '' &&
				productDesc !== '' &&
				productPrice !== '' ) 
			{
				setIsDisabled(false)
			} else {
				setIsDisabled(true)
			}

		}, [productName, productDesc, productPrice])


	function handleProductSubmit (e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDesc,
				price: productPrice,					
				imageUrl: productImageUrl
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === false) {
				let timerInterval
				Swal2.fire({
					title: 'Product Update Error!',
					icon: 'error',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 3000,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})	
				} else {
					let timerInterval
						Swal2.fire({
							title: 'Successfully Updated Product.',
							icon: 'success',
							  html: 'I will close in <b></b> milliseconds.',
							  timer: 3000,
							  timerProgressBar: true,
							  didOpen: () => {
							    Swal2.showLoading()
							    const b = Swal2.getHtmlContainer().querySelector('b')
							    timerInterval = setInterval(() => {
							      b.textContent = Swal2.getTimerLeft()
							    }, 100)
							  },
							  willClose: () => {
							    clearInterval(timerInterval)
							  }
							})

					navigate('/dashboard');	
				}
			})
	}


	// fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
	// 	method: "GET"
	// })
	// .then(response => response.json())
	// .then(data => {
	// 	setProductName(data)		
	// })


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method : 'GET'
		})
		.then(response => response.json())
		.then(data => {
			// setProducts(data);
			// console.log(data);
			setProductName(data.name);
			setProductDesc(data.description);
			setProductPrice(data.price);
			setProductImageUrl(data.imageUrl);

			console.log(data);
		})
}, [])



	return (

		  	  	<Row xl={3} lg={10} md={2} className='mt-3' >
		  	  		<Col className="mx-auto">
						<div className='d-flex'>
							<Button className='btn-sm me-3 mt-2 align-items-center btn-outline-info h-25' as = {Link} to={'/dashboard'}>{`<`}</Button>
		  	  				<h1>Update Product</h1>	
						</div>
		  	  			<Form onSubmit={e => handleProductSubmit(e)}>
		  				     
			  	  				<Form.Group className="mb-3" controlId="formBasicPassword1">
			  	  				  <Form.Label>Product Name</Form.Label>
			  	  				  <Form.Control
			  	  				  type="text"
			  	  				// placeholder={products.name} 
			  	  				// value={pName}
			  	  				  value={productName}
			  	  				  onChange={e => setProductName(e.target.value)}
			  	  				  />
			  	  				</Form.Group>

			  	  				<Form.Group className="mb-3">
			  	  				  <Form.Label>Description</Form.Label>
			  	  				  <Form.Control
			  	  				  type="text" 
			  	  				  value={productDesc}
			  	  				  onChange={e => setProductDesc(e.target.value)}
			  	  				  />
			  	  				</Form.Group>

			  	  				<Form.Group className="mb-3">
			  	  				  <Form.Label>Unit Price</Form.Label>
			  	  				  <Form.Control
			  	  				  type="number" 
			  	  				  value={productPrice}
			  	  				  onChange={e => setProductPrice(e.target.value)}
			  	  				  />
			  	  				</Form.Group>
			  	  				<Form.Group className="mb-3">
			  	  				  <Form.Label>Image URL</Form.Label>
			  	  				  <Form.Control
			  	  				  type="string"
			  	  				  value={productImageUrl}
			  	  				  onChange={e => setProductImageUrl(e.target.value)}
			  	  				  />
			  	  				</Form.Group>
		  				      <Button 
		  				      className="w-100"
		  				      variant="primary" 
		  				      type="submit"
		  				      disabled = {isDisabled}
		  				      >Update Product</Button>	
		  				      
		  	  			</Form>
		  	  		</Col>
		  	  	</Row>

	)
}