import { useState, useEffect } from 'react'

import { Container, Row, Col, Table } from 'react-bootstrap'

export default function UserProfile () {

    const [getUsers, setGetUsers] = useState([]);

    
    // fetch(`${process.env.REACT_APP_API_URL}/users/allusers`, {
    //     headers: {
    //         'Content-Type' : 'application/json',
    //         Authorization : `Bearer ${localStorage.getItem('token')}`
    //     }
    // })
    // .then(response => response.json())
    // .then(data => {
    //     // console.log(data)
    //     setGetUsers(data)
    // })


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/allusers/`, {
            headers: {
                'Content-Type' : 'application/json',
                Authorization : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setGetUsers(data)
        })
    }, [])



    return (

        <Container className='text-center'>
        <Row>
            <Col>
                <h1 className='p-3'>User Profile Page</h1>

                <Table bordered hover>
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>Email</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>isAdmin</th>
                            <th>Mobile No.</th>
                            <th>User Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        {getUsers.map((user) => (
                            <tr key={user._id}>
                                <td>{user._id}</td>
                                <td>{user.email}</td>
                                <td>{user.firstName}</td>
                                <td>{user.lastName}</td>
                                <td>{user.isAdmin ? 'true' : 'false'}</td>
                                <td>{user.mobileNo}</td>
                                <td>{user.createdAt}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>

            </Col>
        </Row>
    </Container>
    )
}