import Container from 'react-bootstrap/Container';
import { Navbar, Nav, Form } from 'react-bootstrap';
import { useState, useEffect,useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOut, faSignIn, faHouse, faDashboard } from '@fortawesome/free-solid-svg-icons';


import UserContext from '../UserContext';
// import { faStarHalfAlt } from '@fortawesome/free-regular-svg-icons';

// The 'as' keyword allows components to be treated as if they are different component gaining access to it's properties and functionalities

// The 'to' keyword is used in place of the 'href' for providing the URL for the page

export default function AppNavBar() {

    const { user }  = useContext(UserContext);

    const [users, setUsers] = useState('');

    // In here, I will capture the value from our localStorage and then store it a state.

    // Syntax:
        // localStorage.getItem('key/property'); 

    // console.log(localStorage.getItem('email'));

    // const [user, setUser] = useState(localStorage.getItem('email'));



    // useEffect(() => {
    //         fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    //             headers: {
    //                 Authorization: `Bearer ${localStorage.getItem('token')}`
    //             }
    //         })
    //         .then(response => response.json())
    //         .then(data => {
    //             setUsers(data)
    //         })
    // }, [])

	return (
                
            <Navbar style={{backgroundColor: 'rgba(2,43,96,255)'}} variant="dark" expand="lg" className="mx-auto px-5 fw-bold fs-5 sticky-top">
                   <Container bg="dark">
                       <Navbar.Brand as = {Link} to = '/' className="brand fw-bold fs-4" >Annie's Boutique</Navbar.Brand>
                       <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                       <Navbar.Collapse id="basic-navbar-nav">
                           <Nav className="ms-auto">
                                 <Nav.Link className='mx-2' as={Link} to="/"><FontAwesomeIcon icon={faHouse} /> Home</Nav.Link>

                                 {
                                   (user.isAdmin) 
                                   ?

                                   <>
                                    <Nav.Link className='mx-2' as={Link} to="/dashboard"><FontAwesomeIcon icon={faDashboard} /> Dashboard</Nav.Link>
                                    <Nav.Link as={Link} to="/products"><FontAwesomeIcon icon={faDashboard} /> Products</Nav.Link>
                                   </>
                                   :
                                   <>
                                   <Nav.Link as={Link} to="/products"><FontAwesomeIcon icon={faDashboard} /> Products</Nav.Link>
                                   </>
                                 }     

                                 {
                                   (user.id && !user.isAdmin) 
                                   ?                
                                      //  <Nav.Link as={Link} to="/cart" hidden>Cart</Nav.Link>
                                    // <Nav.Link className='mx-2' as={Link} to="/products"><FontAwesomeIcon icon={faDashboard} /> Products</Nav.Link>
                                    <Nav.Link as={Link} to="/userorders"><FontAwesomeIcon icon={faDashboard} /> Cart</Nav.Link>
                                   :
                                   <></>
                                 }

                                 {
                                   (user.id != null) 
                                   ?
                                   <>
                                       <Nav.Link className='mx-2' as={Link} to="/logout"><FontAwesomeIcon icon={faSignOut} /></Nav.Link>
                                       {/* <Nav.Link as={Link} to="/logout"><i className="fa-solid fa-user">Logout</i></Nav.Link> */}
                                   </>
                                   :
                                   <>
                                       <Nav.Link as={Link} to="/login"><FontAwesomeIcon icon={faSignIn} /> Sign in</Nav.Link>
                                       {/* <Nav.Link as={Link} to="/register">Register</Nav.Link> */}
                                   </>
                                 }
                           </Nav>
                       </Navbar.Collapse>
                   </Container>
            </Navbar>     

	)
}