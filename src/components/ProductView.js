import { Row, Col, Button, Card } from 'react-bootstrap';
import { useContext } from 'react'
// the useParams allows us to get or extrat the parameter included in our pages
import { Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

import { useState, useEffect } from 'react';

import Swal2 from 'sweetalert2';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function ProductView(){

    const { user }  = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [pImageUrl, setPImageUrl] = useState('');
	const [quantity, setQuantity] = useState(1);
	const [isSubTotal, setIsSubTotal] = useState('');

	const { id } = useParams();


	useEffect(() => {
		document.title = "Single Product"
	}, [])



	const addToCart = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`,{
				method: "POST",
				headers: {
					'Content-Type' : 'application/json',
					Authorization : `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId,
					productName: name,
					productImageUrl : pImageUrl,
					quantity: quantity
					// productId: `${productId}`
				})
			})
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				if(data === true){
				// addToCart();

				let timerInterval
				Swal2.fire({
					title: 'Successfully created your order.',
					icon: 'success',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 2500,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})
					navigate('/products')	
				}
			})
		}

		// const toastCart = () => {
		// 	toast.success('Successfully added to your cart!', {
		// 	position: "top-right",
		// 	autoClose: 2000,
		// 	hideProgressBar: false,
		// 	closeOnClick: true,
		// 	pauseOnHover: true,
		// 	draggable: true,
		// 	progress: undefined,
		// 	theme: "light",
		// 	});
		// }



		useEffect( () => {

				fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
				.then(response => response.json())
				.then(data => {
					// console.log(data)
					setName(data.name);
					setPrice(data.price);
					setDescription(data.description);
					setPImageUrl(data.imageUrl);
				})

			}, [productId])



		function handleQuantitySubmit () {
			setQuantity();
		}



		// const addToCart = () => {
		// 	toast('Hi, Successfully added to your cart!');
		// }


	return (

	// <Row>
	//     <Col>
	//         <Card>
	//             <Card.Body>
	// 				<td><img src={pImageUrl} height="350px" /></td>
	//                 <Card.Title></Card.Title>
	//                 <Card.Title>{name}</Card.Title>
	//                 <Card.Title>{description}</Card.Title>
	//                 <Card.Title>Price: {price}</Card.Title>
	//                 <Card.Title>Quantity: <input type="number" id="quantity" name="quantity" 
	//                 min="1" max="100" step="1" className="text-center" value={quantity} onChange={e => handleQuantitySubmit(e)} /></Card.Title>
	//                 <Button variant="primary" onClick={() => enroll(id)}>Buy</Button>
	//               </Card.Body>
	//          </Card>
	//     </Col>
	// </Row>


	<Card style={{ width: '30rem' }} className="text-center mx-auto">
	      <Card.Img variant="top" className="mx-auto mt-3" style={{ width: '15rem' }} src={pImageUrl} />
	      <Card.Body>
	        <Card.Title><strong>{name}</strong></Card.Title>
	        <Card.Text>{description}</Card.Text>
	        <Card.Text>Price: <strong>&#8369;{price.toLocaleString(undefined, {minimumFractionDigits: 2})}</strong></Card.Text>

	        {
	        	user.id != null ?
				<>
					<Card.Text>Quantity: <input type="number" min="1" max="100" step="1" className="text-center" value={quantity} onChange={e => setQuantity(e.target.value)} /></Card.Text>

	        		<Button variant="primary" className="w-100" onClick={() => addToCart(id)}>Add to cart</Button>
				</>

	        	:
				<>
					<Card.Text>Quantity: <input type="number" min="1" max="100" step="1" className="text-center" value={quantity} onChange={e => setQuantity(e.target.value)} disabled/></Card.Text>

	        		<Button variant="primary" className="w-100" as ={Link} to={'/login'}>Login in to your account.</Button>
				</>
	        }
	      </Card.Body>
	       
	       <ToastContainer />
	    </Card>

	    // <ToastContainer />
	)
}