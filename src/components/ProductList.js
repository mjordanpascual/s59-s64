import { Button, Table } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'
import { Link, useNavigate
 } from 'react-router-dom';

// import ProductCard from "../components/ProductCard";

// import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function ProductList () {

    // const { user }  = useContext(UserContext);

	const [products, setProducts] = useState([]);

	// const navigate = useNavigate();
	

	function archiveProduct (productId, isActive) {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PATCH",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data) {
				let timerInterval
				Swal2.fire({
					title: 'Product Archived!',
					icon: 'success',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 1500,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})	
				.then( () => {window.location.reload();})
			}
		})
	}


	function unArchiveProduct (productId, isActive) {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PATCH",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data) {
				let timerInterval
				Swal2.fire({
					title: 'Product Unarchived!',
					icon: 'success',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 1500,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})	
				.then( () => {window.location.reload();})
			}
		})
	}


	function handleDeleteProduct (productId) {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "DELETE",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
				  title: 'Are you sure you want to delete?',
				  text: "You won't be able to revert this!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
				  if (result.isConfirmed) {
				    Swal2.fire(
				      'Product Successfully Deleted!',
				      'Your Product has been deleted.',
				      'success'
				    )
				.then(() => {
					window.location.reload();
				})
				  }
				})
			}
		})
	}


	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(response => response.json())
			.then(data => {
				let sortProduct = data.sort((item1, item2) => (item1.name > item2.name) ? 1 : - 1);
				setProducts(sortProduct);
			})
	}, [])


	return (

		<Table xs={12} md={3} className="text-center" style={{verticalAlign: 'middle'}} hover responsive bordered>
			<thead>
				<tr>
					<th>#</th>
					<th>Product Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Image</th>
					<th>Stocks</th>
					<th>Availability</th>
					<th colSpan={3}>Actions</th>
				</tr>
			</thead>

			<tbody >
				{products.map((product, index) => (
					<tr key={product._id}>
						<td>{index + 1}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>&#8369;{product.price.toLocaleString(undefined, {minimumFractionDigits: 2})}</td>
						<td><img src={product.imageUrl} height="100px" /></td>
						<td>***</td>
						<td>{product.isActive ? 'Available' : 'Unavailable'}</td>

						<td>
															
							{
								(product.isActive === true)
							?
								<Button variant="outline-warning" className="btn-sm" onClick={ () => archiveProduct(product._id, 
									false) }>Archive</Button> 
							:
								<Button variant="outline-success" className="btn-sm" onClick={ () => unArchiveProduct(product._id, true) }>Unarchive</Button>
							}
						</td>

						<td><Button variant="outline-info" as={ Link } to={`update/${product._id}`} className="btn-sm">Update</Button></td>
						<td><Button variant="outline-danger"  to={`update/${product._id}`} className="btn-sm" onClick={e => handleDeleteProduct(product._id) }>Delete</Button></td>	
					</tr>					
				))}
			</tbody>			
		</Table>

	)
}