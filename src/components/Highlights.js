import { Row, Col, Card } from 'react-bootstrap';

export default function Highlight() {
	return (

		<Row className="mt-3 mb-3 text-center">
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card>
                            <img src="https://images.footlocker.com/content/dam/final/footlocker/site/homepage/2023/june/230607-fl-shop-by-category-mens.jpg" alt="Men's" />
                        </Card>
                        <Card.Title className="mt-3">
                            <h2>Men's</h2>
                        </Card.Title>
                        {/*<Card.Text>
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim.
                        </Card.Text>*/}
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                           <Card className="cardHighlight p-3">
                               <Card.Body>
                                   <Card>
                                       <img src="https://images.footlocker.com/content/dam/final/footlocker/site/homepage/2023/june/230607-fl-shop-by-category-womens.jpg" alt="Men's" />
                                   </Card>
                                   <Card.Title className="mt-3">
                                       <h2>Women's</h2>
                                   </Card.Title>
                                   {/*<Card.Text>
                                       Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim.
                                   </Card.Text>*/}
                               </Card.Body>
                           </Card>
                       </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card>
                        <img src="https://images.footlocker.com/content/dam/final/footlocker/site/homepage/2023/june/230607-fl-shop-by-category-kids.jpg" alt="Accessories" />
                    </Card>
                        <Card.Title className="mt-3">
                            <h2>Kid's</h2>
                        </Card.Title>
                        {/*<Card.Text>
                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur.
                        </Card.Text>*/}
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card>
                        <img src="https://images.footlocker.com/content/dam/final/footlocker/site/homepage/2023/june/230607-fl-shop-by-category-clothing.jpg" alt="Accessories" />
                    </Card>
                        <Card.Title className="mt-3">
                            <h2>Clothing</h2>
                        </Card.Title>
                        {/*<Card.Text>
                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur.
                        </Card.Text>*/}
                    </Card.Body>
                </Card>
            </Col>
        </Row>

	)
}