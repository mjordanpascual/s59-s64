import { Button, Row, Col, Card, Container, InputGroup } from 'react-bootstrap';

import DarkVariantExample from './DarkVariantExample'

import { Image } from 'react-bootstrap'

import { Link } from 'react-router-dom';

import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

import StoreImg from '../assets/storebgk.png';

export default function Banner () {

	const { user } = useContext(UserContext);


	// const bgImage = {
	//     backgroundImage: 'url(https://smallbiztrends.com/ezoimgfmt/media.smallbiztrends.com/2022/11/best-products-to-sell-online.png?ezimgfmt=ng%3Awebp%2Fngcb12%2Frs%3Adevice%2Frscb12-1)',
	//     minHeight: '45vh',
	//     backgroundRepeat: 'no-repeat',
	//     backgroundSize: 'cover',
	//     backgroundPosition: '75% 50%',
	//     backgroundAttachment: 'scroll',
	//     display: 'absolute'
	// }

	// const frontBg = {
	//     background: 'rgba(0, 0, 0, 0.5)',
	//     minHeight: '45vh',
	// }

	// const container = {
	//     // textAlign: 'center',
	//     marginLeft: '1em',
	//     position: 'relative',
	//     width: '60%',
	//     borderRadius: '15px'
	// }

	// const title = {
	//     fontFamily: 'Zilla Slab",serif',
	//     fontSize: '4',
	//     fontWeight: '600',
	//     textAlign: 'left',
	//     display: 'inline-block',
	//     marginTop: '1em',
	//     marginLeft: 'auto',
	//     color: 'white'
	// }

	// const subtitle = {
	//     fontSize: '2',
	//     fontFamily: "Open Sans, sans-serif",
	//     fontWeight: '300',
	//     color: 'white',
	//     textAlign: 'l00%',
	//     width: '46rem',
	//     marginTop: '5px'
	// }

	// const applyButton = {
	//     // color: 'white',
	//     padding: '10px',
	//     width: '180px',
	//     textAlign: 'center',
	//     fontSize: '1rem',
	//     fontFamily: 'Open Sans, sans-serif',
	//     fontWeight: '600',
	//     // backgroundColor: '#c2195b',
	//     // borderColor: 'transparent',
	//     borderRadius: '25px',
	//     // marginLeft: '28%',
	//     marginTop: '2.5rem',
	//     boxShadow: '0 2px 3px rgb(0 0 0 / 100%)',
	// }


	useEffect(() => {
		document.title = "E-commerce App";
	}, [])

	return (
		// <Row>
	    //     <Col className="p-5 bg-light">
		//     <img src="https://smallbiztrends.com/ezoimgfmt/media.smallbiztrends.com/2022/11/best-products-to-sell-online.png?ezimgfmt=ng%3Awebp%2Fngcb12%2Frs%3Adevice%2Frscb12-1" class="opacity-50 d-inline" style={{minHeight: "45vh"}} alt="Banner" cover />
	    //         <h1 className="display-4">Amazing E-Commerce Online Shop</h1>
	    //         <p className="display-5">Platform for Everyone.</p>
	    //         <Button as = {Link} to = '/products' variant="primary">Shop now!</Button>
	    //     </Col>
	    // </Row>

			<Container className='col-xl-12 col-lg-8 col-md-6 bg-dark text-white rounded'>
				<Row style={{height: '90vh'}} className="align-items-center">
					<Col className='col-xl-6 p-5 m-0 text-left'>
						<div style={{margin: '4em'}}>
							<h1>Make your fashion more perfect</h1>
							<p>With our clothing collection, you can take your style to the next level</p>
							<Button as = {Link} to = '/products'>Shop now!</Button>
						</div>
					</Col>
					<Col className='col-xl-6 col-md-12 text-center col-sm-12 text-right'>
						<div>
							<Image style={{width: '35rem'}} className="p-0 mr-5 rounded" src={StoreImg} />
						</div>
					</Col>
				</Row>
		</Container>
		

	)
}